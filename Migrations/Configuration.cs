namespace Transportation_Module.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Transportation_Module.Models;
    using System.Globalization;

    internal sealed class Configuration : DbMigrationsConfiguration<Transportation_Module.Models.Transportation_ModuleContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Transportation_Module.Models.Transportation_ModuleContext context)
        {
            context.CfgBus.AddOrUpdate(x => x.BusName,
                                         new CfgBus { BusId = "1", BusName = "Bus 1", Capacity = 40 },
                                         new CfgBus { BusId = "2", BusName = "Bus 2", Capacity = 40 },
                                         new CfgBus { BusId = "3", BusName = "Bus 3", Capacity = 40 },
                                         new CfgBus { BusId = "4", BusName = "Bus 4", Capacity = 40 },
                                         new CfgBus { BusId = "5", BusName = "Bus 5", Capacity = 32 },
                                         new CfgBus { BusId = "6", BusName = "Bus 6", Capacity = 40 },
                                         new CfgBus { BusId = "7", BusName = "Bus 7", Capacity = 22 },
                                         new CfgBus { BusId = "8", BusName = "Bus 8", Capacity = 40 },
                                         new CfgBus { BusId = "9", BusName = "Bus 9", Capacity = 55 },
                                         new CfgBus { BusId = "10", BusName = "Bus 10", Capacity = 40 },
                                         new CfgBus { BusId = "11", BusName = "Bus 11", Capacity = 37 },
                                         new CfgBus { BusId = "12", BusName = "Bus 12", Capacity = 40 }
                                         );
            context.SaveChanges();

            context.CfgZones.AddOrUpdate(x => x.ZoneName,
                                        new CfgZone { ZoneName = "Zone 1", ZoneOrder = 1 },
                                        new CfgZone { ZoneName = "Zone 2", ZoneOrder = 2 },
                                        new CfgZone { ZoneName = "Zone 3", ZoneOrder = 3 },
                                        new CfgZone { ZoneName = "Zone 4", ZoneOrder = 4 }
                                        );
            context.SaveChanges();

            context.CfgBusSets.AddOrUpdate(x => x.BusSetName,
                                        new CfgBusSet { BusSetName = "BusSet 1", BusSetOrder = 1, AMAssignment = true, PMAssignment = false, DateStart = DateTime.ParseExact("05-01-2019", "MM/dd/yyyy", CultureInfo.InvariantCulture), DateEnd = DateTime.ParseExact("07-31-2019", "MM-dd-yyyy", CultureInfo.InvariantCulture) },
                                        new CfgBusSet { BusSetName = "BusSet 2", BusSetOrder = 2, AMAssignment = false, PMAssignment = true, DateStart = DateTime.ParseExact("05-01-2019", "MM/dd/yyyy", CultureInfo.InvariantCulture), DateEnd = DateTime.ParseExact("07-31-2019", "MM-dd-yyyy", CultureInfo.InvariantCulture) }
                                        );
            context.SaveChanges();

            // Fetch the "Bus 1" role
            CfgBus Bus1 = context.CfgBus.Single(r => r.BusName == "Bus 1");
            CfgBus Bus2 = context.CfgBus.Single(r => r.BusName == "Bus 2");
            CfgBus Bus3 = context.CfgBus.Single(r => r.BusName == "Bus 3");
            CfgBus Bus4 = context.CfgBus.Single(r => r.BusName == "Bus 4");
            CfgBus Bus5 = context.CfgBus.Single(r => r.BusName == "Bus 5");
            CfgBus Bus6 = context.CfgBus.Single(r => r.BusName == "Bus 6");
            CfgBus Bus7 = context.CfgBus.Single(r => r.BusName == "Bus 7");
            CfgBus Bus8 = context.CfgBus.Single(r => r.BusName == "Bus 8");
            CfgBus Bus9 = context.CfgBus.Single(r => r.BusName == "Bus 9");
            CfgBus Bus10 = context.CfgBus.Single(r => r.BusName == "Bus 10");
            CfgBus Bus11 = context.CfgBus.Single(r => r.BusName == "Bus 11");
            CfgBus Bus12 = context.CfgBus.Single(r => r.BusName == "Bus 12");

            CfgZone Zone1 = context.CfgZones.Single(r => r.ZoneName == "Zone 1");
            CfgZone Zone2 = context.CfgZones.Single(r => r.ZoneName == "Zone 2");
            CfgZone Zone3 = context.CfgZones.Single(r => r.ZoneName == "Zone 3");
            CfgZone Zone4 = context.CfgZones.Single(r => r.ZoneName == "Zone 4");

            CfgBusSet BusSet1 = context.CfgBusSets.Single(r => r.BusSetName == "BusSet 1");
            CfgBusSet BusSet2 = context.CfgBusSets.Single(r => r.BusSetName == "BusSet 2");

            context.CfgBusZoneAssignments.AddOrUpdate(
                                        new CfgBusZoneAssignments { BusId = Bus1.BusId, ZoneId = Zone1.ZoneId, BusSetId = BusSet1.BusSetId },
                                        new CfgBusZoneAssignments { BusId = Bus2.BusId, ZoneId = Zone1.ZoneId, BusSetId = BusSet1.BusSetId },
                                        new CfgBusZoneAssignments { BusId = Bus3.BusId, ZoneId = Zone1.ZoneId, BusSetId = BusSet1.BusSetId },
                                        new CfgBusZoneAssignments { BusId = Bus4.BusId, ZoneId = Zone2.ZoneId, BusSetId = BusSet1.BusSetId },
                                        new CfgBusZoneAssignments { BusId = Bus5.BusId, ZoneId = Zone2.ZoneId, BusSetId = BusSet1.BusSetId },
                                        new CfgBusZoneAssignments { BusId = Bus6.BusId, ZoneId = Zone2.ZoneId, BusSetId = BusSet1.BusSetId },
                                        new CfgBusZoneAssignments { BusId = Bus7.BusId, ZoneId = Zone3.ZoneId, BusSetId = BusSet1.BusSetId },
                                        new CfgBusZoneAssignments { BusId = Bus8.BusId, ZoneId = Zone3.ZoneId, BusSetId = BusSet1.BusSetId },
                                        new CfgBusZoneAssignments { BusId = Bus9.BusId, ZoneId = Zone3.ZoneId, BusSetId = BusSet1.BusSetId },
                                        new CfgBusZoneAssignments { BusId = Bus10.BusId, ZoneId = Zone4.ZoneId, BusSetId = BusSet1.BusSetId },
                                        new CfgBusZoneAssignments { BusId = Bus11.BusId, ZoneId = Zone4.ZoneId, BusSetId = BusSet1.BusSetId },
                                        new CfgBusZoneAssignments { BusId = Bus12.BusId, ZoneId = Zone4.ZoneId, BusSetId = BusSet1.BusSetId },
                                        new CfgBusZoneAssignments { BusId = Bus1.BusId, ZoneId = Zone1.ZoneId, BusSetId = BusSet2.BusSetId },
                                        new CfgBusZoneAssignments { BusId = Bus2.BusId, ZoneId = Zone1.ZoneId, BusSetId = BusSet2.BusSetId },
                                        new CfgBusZoneAssignments { BusId = Bus3.BusId, ZoneId = Zone1.ZoneId, BusSetId = BusSet2.BusSetId },
                                        new CfgBusZoneAssignments { BusId = Bus4.BusId, ZoneId = Zone2.ZoneId, BusSetId = BusSet2.BusSetId },
                                        new CfgBusZoneAssignments { BusId = Bus5.BusId, ZoneId = Zone2.ZoneId, BusSetId = BusSet2.BusSetId },
                                        new CfgBusZoneAssignments { BusId = Bus6.BusId, ZoneId = Zone2.ZoneId, BusSetId = BusSet2.BusSetId },
                                        new CfgBusZoneAssignments { BusId = Bus7.BusId, ZoneId = Zone3.ZoneId, BusSetId = BusSet2.BusSetId },
                                        new CfgBusZoneAssignments { BusId = Bus8.BusId, ZoneId = Zone3.ZoneId, BusSetId = BusSet2.BusSetId },
                                        new CfgBusZoneAssignments { BusId = Bus9.BusId, ZoneId = Zone3.ZoneId, BusSetId = BusSet2.BusSetId },
                                        new CfgBusZoneAssignments { BusId = Bus10.BusId, ZoneId = Zone4.ZoneId, BusSetId = BusSet2.BusSetId },
                                        new CfgBusZoneAssignments { BusId = Bus11.BusId, ZoneId = Zone4.ZoneId, BusSetId = BusSet2.BusSetId },
                                        new CfgBusZoneAssignments { BusId = Bus12.BusId, ZoneId = Zone4.ZoneId, BusSetId = BusSet2.BusSetId }
                                        );

            context.SaveChanges();
        }
    }
}
