namespace Transportation_Module.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.cfgBus",
                c => new
                    {
                        BusId = c.String(nullable: false, maxLength: 128),
                        BusName = c.String(nullable: false, maxLength: 250),
                        Capacity = c.Int(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        EditDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.BusId);
            
            CreateTable(
                "dbo.cfgBusZoneAssignments",
                c => new
                    {
                        BusId = c.String(nullable: false, maxLength: 128),
                        ZoneId = c.String(nullable: false, maxLength: 128),
                        BusSetId = c.String(nullable: false, maxLength: 128),
                        BusZoneAssignmentId = c.String(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        EditDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => new { t.BusId, t.ZoneId, t.BusSetId })
                .ForeignKey("dbo.cfgBus", t => t.BusId, cascadeDelete: true)
                .ForeignKey("dbo.cfgBusSet", t => t.BusSetId, cascadeDelete: true)
                .ForeignKey("dbo.cfgZone", t => t.ZoneId, cascadeDelete: true)
                .Index(t => t.BusId)
                .Index(t => t.ZoneId)
                .Index(t => t.BusSetId);
            
            CreateTable(
                "dbo.cfgBusSet",
                c => new
                    {
                        BusSetId = c.String(nullable: false, maxLength: 128),
                        BusSetName = c.String(nullable: false, maxLength: 250),
                        BusSetOrder = c.Int(nullable: false),
                        DateStart = c.DateTime(nullable: false),
                        DateEnd = c.DateTime(nullable: false),
                        AMAssignment = c.Boolean(nullable: false),
                        PMAssignment = c.Boolean(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        EditDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.BusSetId);
            
            CreateTable(
                "dbo.cfgZone",
                c => new
                    {
                        ZoneId = c.String(nullable: false, maxLength: 128),
                        ZoneName = c.String(nullable: false, maxLength: 250),
                        ZoneOrder = c.Int(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        EditDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ZoneId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.cfgBusZoneAssignments", "ZoneId", "dbo.cfgZone");
            DropForeignKey("dbo.cfgBusZoneAssignments", "BusSetId", "dbo.cfgBusSet");
            DropForeignKey("dbo.cfgBusZoneAssignments", "BusId", "dbo.cfgBus");
            DropIndex("dbo.cfgBusZoneAssignments", new[] { "BusSetId" });
            DropIndex("dbo.cfgBusZoneAssignments", new[] { "ZoneId" });
            DropIndex("dbo.cfgBusZoneAssignments", new[] { "BusId" });
            DropTable("dbo.cfgZone");
            DropTable("dbo.cfgBusSet");
            DropTable("dbo.cfgBusZoneAssignments");
            DropTable("dbo.cfgBus");
        }
    }
}
