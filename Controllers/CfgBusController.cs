﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Transportation_Module.Models;

namespace Transportation_Module.Controllers
{
    public class CfgBusController : ApiController
    {
        private Transportation_ModuleContext db = new Transportation_ModuleContext();

        // GET: api/CfgBus
        public IQueryable<CfgBus> GetCfgBus()
        {
            return db.CfgBus;
        }

        // GET: api/CfgBus/5
        [ResponseType(typeof(CfgBus))]
        public async Task<IHttpActionResult> GetCfgBus(string id)
        {
            CfgBus cfgBus = await db.CfgBus.FindAsync(id);
            if (cfgBus == null)
            {
                return NotFound();
            }

            return Ok(cfgBus);
        }

        // PUT: api/CfgBus/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutCfgBus(string id, CfgBus cfgBus)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != cfgBus.BusId)
            {
                return BadRequest();
            }

            db.Entry(cfgBus).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CfgBusExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/CfgBus
        [ResponseType(typeof(CfgBus))]
        public async Task<IHttpActionResult> PostCfgBus(CfgBus cfgBus)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.CfgBus.Add(cfgBus);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = cfgBus.BusId }, cfgBus);
        }

        // DELETE: api/CfgBus/5
        [ResponseType(typeof(CfgBus))]
        public async Task<IHttpActionResult> DeleteCfgBus(string id)
        {
            CfgBus cfgBus = await db.CfgBus.FindAsync(id);
            if (cfgBus == null)
            {
                return NotFound();
            }

            db.CfgBus.Remove(cfgBus);
            await db.SaveChangesAsync();

            return Ok(cfgBus);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CfgBusExists(string id)
        {
            return db.CfgBus.Count(e => e.BusId == id) > 0;
        }
    }
}