﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Transportation_Module.Models;

namespace Transportation_Module.Controllers
{
    public class CfgBusZoneAssignmentsController : ApiController
    {
        private Transportation_ModuleContext db = new Transportation_ModuleContext();

        // GET: api/CfgBusZoneAssignments
        public IQueryable<CfgBusZoneAssignments> GetCfgBusZoneAssignments()
        {
            return db.CfgBusZoneAssignments;
        }

        // GET: api/CfgBusZoneAssignments/5
        [ResponseType(typeof(CfgBusZoneAssignments))]
        public async Task<IHttpActionResult> GetCfgBusZoneAssignments(string id)
        {
            CfgBusZoneAssignments cfgBusZoneAssignments = await db.CfgBusZoneAssignments.FindAsync(id);
            if (cfgBusZoneAssignments == null)
            {
                return NotFound();
            }

            return Ok(cfgBusZoneAssignments);
        }

        // PUT: api/CfgBusZoneAssignments/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutCfgBusZoneAssignments(string id, CfgBusZoneAssignments cfgBusZoneAssignments)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != cfgBusZoneAssignments.BusId)
            {
                return BadRequest();
            }

            db.Entry(cfgBusZoneAssignments).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CfgBusZoneAssignmentsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/CfgBusZoneAssignments
        [ResponseType(typeof(CfgBusZoneAssignments))]
        public async Task<IHttpActionResult> PostCfgBusZoneAssignments(CfgBusZoneAssignments cfgBusZoneAssignments)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.CfgBusZoneAssignments.Add(cfgBusZoneAssignments);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (CfgBusZoneAssignmentsExists(cfgBusZoneAssignments.BusId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = cfgBusZoneAssignments.BusId }, cfgBusZoneAssignments);
        }

        // DELETE: api/CfgBusZoneAssignments/5
        [ResponseType(typeof(CfgBusZoneAssignments))]
        public async Task<IHttpActionResult> DeleteCfgBusZoneAssignments(string id)
        {
            CfgBusZoneAssignments cfgBusZoneAssignments = await db.CfgBusZoneAssignments.FindAsync(id);
            if (cfgBusZoneAssignments == null)
            {
                return NotFound();
            }

            db.CfgBusZoneAssignments.Remove(cfgBusZoneAssignments);
            await db.SaveChangesAsync();

            return Ok(cfgBusZoneAssignments);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CfgBusZoneAssignmentsExists(string id)
        {
            return db.CfgBusZoneAssignments.Count(e => e.BusId == id) > 0;
        }
    }
}