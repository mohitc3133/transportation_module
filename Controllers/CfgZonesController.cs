﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Transportation_Module.Models;

namespace Transportation_Module.Controllers
{
    public class CfgZonesController : ApiController
    {
        private Transportation_ModuleContext db = new Transportation_ModuleContext();

        // GET: api/CfgZones
        public IQueryable<CfgZone> GetCfgZones()
        {
            return db.CfgZones;
        }

        // GET: api/CfgZones/5
        [ResponseType(typeof(CfgZone))]
        public async Task<IHttpActionResult> GetCfgZone(string id)
        {
            CfgZone cfgZone = await db.CfgZones.FindAsync(id);
            if (cfgZone == null)
            {
                return NotFound();
            }

            return Ok(cfgZone);
        }

        // PUT: api/CfgZones/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutCfgZone(string id, CfgZone cfgZone)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != cfgZone.ZoneId)
            {
                return BadRequest();
            }

            db.Entry(cfgZone).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CfgZoneExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/CfgZones
        [ResponseType(typeof(CfgZone))]
        public async Task<IHttpActionResult> PostCfgZone(CfgZone cfgZone)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.CfgZones.Add(cfgZone);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = cfgZone.ZoneId }, cfgZone);
        }

        // DELETE: api/CfgZones/5
        [ResponseType(typeof(CfgZone))]
        public async Task<IHttpActionResult> DeleteCfgZone(string id)
        {
            CfgZone cfgZone = await db.CfgZones.FindAsync(id);
            if (cfgZone == null)
            {
                return NotFound();
            }

            db.CfgZones.Remove(cfgZone);
            await db.SaveChangesAsync();

            return Ok(cfgZone);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CfgZoneExists(string id)
        {
            return db.CfgZones.Count(e => e.ZoneId == id) > 0;
        }
    }
}