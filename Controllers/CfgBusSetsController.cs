﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Transportation_Module.Models;

namespace Transportation_Module.Controllers
{
    public class CfgBusSetsController : ApiController
    {
        private Transportation_ModuleContext db = new Transportation_ModuleContext();

        // GET: api/CfgBusSets
        public IQueryable<CfgBusSet> GetCfgBusSets()
        {
            return db.CfgBusSets;
        }

        // GET: api/CfgBusSets/5
        [ResponseType(typeof(CfgBusSet))]
        public async Task<IHttpActionResult> GetCfgBusSet(string id)
        {
            CfgBusSet cfgBusSet = await db.CfgBusSets.FindAsync(id);
            if (cfgBusSet == null)
            {
                return NotFound();
            }

            return Ok(cfgBusSet);
        }

        // PUT: api/CfgBusSets/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutCfgBusSet(string id, CfgBusSet cfgBusSet)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != cfgBusSet.BusSetId)
            {
                return BadRequest();
            }

            db.Entry(cfgBusSet).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CfgBusSetExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/CfgBusSets
        [ResponseType(typeof(CfgBusSet))]
        public async Task<IHttpActionResult> PostCfgBusSet(CfgBusSet cfgBusSet)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.CfgBusSets.Add(cfgBusSet);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = cfgBusSet.BusSetId }, cfgBusSet);
        }

        // DELETE: api/CfgBusSets/5
        [ResponseType(typeof(CfgBusSet))]
        public async Task<IHttpActionResult> DeleteCfgBusSet(string id)
        {
            CfgBusSet cfgBusSet = await db.CfgBusSets.FindAsync(id);
            if (cfgBusSet == null)
            {
                return NotFound();
            }

            db.CfgBusSets.Remove(cfgBusSet);
            await db.SaveChangesAsync();

            return Ok(cfgBusSet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CfgBusSetExists(string id)
        {
            return db.CfgBusSets.Count(e => e.BusSetId == id) > 0;
        }
    }
}