﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Transportation_Module.Models
{
    [Table("cfgBus")]
    public class CfgBus
    {   [Key]
        [DatabaseGenerated(databaseGeneratedOption: DatabaseGeneratedOption.Identity)]
        public String BusId { get; set; }

        [Required]
        [MaxLength(250, ErrorMessage = "Exceeds the Maximum length allowed"), MinLength(1, ErrorMessage = "Bus Name Missing"), ConcurrencyCheck]
        [Display(Name = "Bus Name")]
        public string BusName { get; set; }

        [Required]
        [Display(Name = "Bus Capacity")]
        [ConcurrencyCheck]
        public int Capacity { get; set; }

        public virtual ICollection<CfgBusZoneAssignments> BusZoneAssignments { get; set; }


        [Display(Name = "Create Date")]
        [DatabaseGenerated(databaseGeneratedOption: DatabaseGeneratedOption.Computed)]
        public DateTime CreateDate { get; set; } = DateTime.UtcNow;

        [Display(Name = "Edit Date")]
        public DateTime EditDate { get; set; } = DateTime.UtcNow;
    }
}