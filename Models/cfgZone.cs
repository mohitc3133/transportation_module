﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Transportation_Module.Models
{
    [Table("cfgZone")]
    public class CfgZone
    {   
        [Key]
        [DatabaseGenerated(databaseGeneratedOption: DatabaseGeneratedOption.Identity)]
        public string ZoneId { get; set; }

        [Required]
        [MaxLength(250, ErrorMessage = "Exceeds the Maximum length allowed"), MinLength(1, ErrorMessage = "Bus Name Missing"), ConcurrencyCheck]
        [Display(Name ="Zone Name")]
        public string ZoneName { get; set; }

        [Required]
        public int ZoneOrder { get; set; }

        public virtual ICollection<CfgBusZoneAssignments> BusZoneAssignments { get; set; }


        [Display(Name = "Create Date")]
        [DatabaseGenerated(databaseGeneratedOption: DatabaseGeneratedOption.Computed)]
        public DateTime CreateDate { get; set; } = DateTime.UtcNow;

        [Display(Name = "Edit Date")]
        public DateTime EditDate { get; set; } = DateTime.UtcNow;

    }
}