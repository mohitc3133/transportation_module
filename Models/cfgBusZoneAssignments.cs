﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Transportation_Module.Models
{
    [Table("cfgBusZoneAssignments")]
    public class CfgBusZoneAssignments
    {   
        
        [DatabaseGenerated(databaseGeneratedOption: DatabaseGeneratedOption.Identity)]
        public string BusZoneAssignmentId { get; set; } 

        [Key]
        [Column(Order = 1)]
        public string BusId { get; set; }
        [ForeignKey("BusId")]
        public virtual CfgBus Bus { get; set; }

        [Key]
        [Column(Order = 2)]
        public string ZoneId { get; set; }
        [ForeignKey("ZoneId")]
        public virtual CfgZone Zone { get; set; }

        [Key]
        [Column(Order = 3)]
        public string BusSetId { get; set; }
        [ForeignKey("BusSetId")]
        public virtual CfgBusSet BusSet { get; set; }

        [Display(Name = "Create Date")]
        [DatabaseGenerated(databaseGeneratedOption: DatabaseGeneratedOption.Computed)]
        public DateTime CreateDate { get; set; } = DateTime.UtcNow;

        [Display(Name = "Edit Date")]
        public DateTime EditDate { get; set; } = DateTime.UtcNow;

    }
}