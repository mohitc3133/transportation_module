﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Transportation_Module.Models
{
    [Table("cfgBusSet")]
    public class CfgBusSet
    {   [Key]
        [DatabaseGenerated(databaseGeneratedOption:DatabaseGeneratedOption.Identity)]
        public string BusSetId { get; set; }

        [Required]
        [MaxLength(250, ErrorMessage = "Exceeds the Maximum length allowed"),MinLength(1, ErrorMessage = "Bus Set Name Missing"), ConcurrencyCheck]
        [Display(Name ="Bus Set Name")]
        public string BusSetName { get; set; }

        public int BusSetOrder { get; set; }

        public virtual ICollection<CfgBusZoneAssignments> BusZoneAssignments { get; set; }


        [Display(Name ="Start Date")]
        [ConcurrencyCheck]
        public DateTime DateStart { get; set; }

        [Display(Name = "End Date")]
        [ConcurrencyCheck]
        public DateTime DateEnd { get; set; }

        [Display(Name ="AM")]
        [ConcurrencyCheck]
        public Boolean AMAssignment { get; set; }

        [Display(Name = "PM")]
        [ConcurrencyCheck]
        public Boolean PMAssignment { get; set; }

        [Display(Name = "Create Date")]
        [DatabaseGenerated(databaseGeneratedOption: DatabaseGeneratedOption.Computed)]
        public DateTime CreateDate { get; set; } = DateTime.UtcNow;

        [Display(Name = "Edit Date")]
        public DateTime EditDate { get; set; } = DateTime.UtcNow;

    }
}